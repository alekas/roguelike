package com.spadusa.roguelike.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.spadusa.roguelike.RoguelikeGame;

public abstract class AbstractScreen implements com.badlogic.gdx.Screen
{
	protected RoguelikeGame game;
	protected SpriteBatch batch;
	protected BitmapFont font;
	
	public AbstractScreen(RoguelikeGame game)
	{
		this.game = game;
		this.batch = new SpriteBatch();
		this.font = new BitmapFont();
		Gdx.input.setInputProcessor(new AbstractScreen.InputProcessor());
	}
	
	@Override
	public void show()
	{
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		batch.begin();
		font.setColor(Color.WHITE);
		font.draw(batch, String.valueOf(Gdx.graphics.getFramesPerSecond()) + " FPS", 10, Gdx.graphics.getHeight());
		batch.end();
		
		update(delta);
	}
	
	protected boolean handleKeyDown(int keycode)
	{
		return false;
	}
	
	protected boolean handleKeyUp(int keycode)
	{
		return false;
	}
	
	protected abstract void update(float delta);
	
	private class InputProcessor implements com.badlogic.gdx.InputProcessor
	{

		@Override
		public boolean keyDown(int keycode)
		{
			return handleKeyDown(keycode);
		}

		@Override
		public boolean keyUp(int keycode)
		{
			return handleKeyUp(keycode);
		}

		@Override
		public boolean keyTyped(char character)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer, int button)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean mouseMoved(int screenX, int screenY)
		{
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean scrolled(int amount)
		{
			// TODO Auto-generated method stub
			return false;
		}

	}
	
	@Override
	public void hide()
	{
		dispose();
	}
}
