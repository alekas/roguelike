package com.spadusa.roguelike.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.spadusa.roguelike.RoguelikeGame;
import com.spadusa.roguelike.config.Config;
import com.spadusa.roguelike.model.Entity;
import com.spadusa.roguelike.world.World;
import com.spadusa.roguelike.collision.Collidable;

public class GameScreen extends AbstractScreen
{
	protected OrthographicCamera camera;
	protected OrthographicCamera debugCamera;
	protected OrthographicCamera hudCamera;
	protected Entity player;
	protected World world;
	protected Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();
	float velocity_x=0, velocity_y=0;
	protected Vector2 viewport;
	
	public GameScreen(RoguelikeGame game)
	{
		super(game);
		world = new World(Config.World.WIDTH, Config.World.HEIGHT);
		viewport = new Vector2(Gdx.graphics.getWidth()/Config.Box2D.PIXELS_PER_METER, Gdx.graphics.getHeight()/Config.Box2D.PIXELS_PER_METER);
		camera = new OrthographicCamera(viewport.x, viewport.y);
		//debugCamera = new OrthographicCamera(viewport.x, viewport.y);
		hudCamera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		hudCamera.position.set(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 0);
		hudCamera.update();
		
		player = new Entity(world, '@');
		player.getController().initPhysics(viewport.x/2, viewport.y/2, 1f, 1.7f, world, BodyType.DynamicBody);
		
		createContactListener();
	}
	
	public void createContactListener()
	{
		world.getBox2DWorld().setContactListener(new ContactListener() {

			@Override
			public void beginContact(Contact contact)
			{
				Collidable a = (Collidable) contact.getFixtureA().getBody().getUserData();
				Collidable b = (Collidable) contact.getFixtureB().getBody().getUserData();
				
				a.collisionStart(b);
				b.collisionStart(a);
			}

			@Override
			public void endContact(Contact contact)
			{
				Object a = contact.getFixtureA().getBody().getUserData();
				Object b = contact.getFixtureB().getBody().getUserData();
				
				if(a instanceof Collidable && b instanceof Collidable)
				{
					((Collidable)a).collisionStart(b);
					((Collidable)b).collisionStart(a);
				}
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold)
			{
				// TODO Auto-generated method stub
				
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse)
			{
				// TODO Auto-generated method stub
				
			}});
	}

	@Override
	public void resize(int width, int height)
	{
		camera = new OrthographicCamera(width/Config.Box2D.PIXELS_PER_METER, height/Config.Box2D.PIXELS_PER_METER);
		hudCamera = new OrthographicCamera(width, height);
		hudCamera.position.set(width/2, height/2, 0);
		hudCamera.update();
	}

	@Override
	public void show()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}


	@Override
	protected void update(float delta)
	{
		world.update(delta);
		//handleInput();
		player.getController().update(delta);
		player.setVelocity(velocity_x, velocity_y);
		float x=player.getPosition().x, y=player.getPosition().y;
		
		if(x<viewport.x/2)
			x = viewport.x/2;
		if(x>world.getWidth() - viewport.x/2)
			x = world.getWidth() - viewport.x/2;

		if(y<viewport.y/2)
			y = viewport.y/2;
		if(y>world.getHeight() - viewport.y/2)
			y = world.getHeight() - viewport.y/2;
		camera.position.set(x, y, 0);
		camera.update();
	}

	@Override
	protected boolean handleKeyDown(int keycode)
	{
		switch(keycode)
		{
			case Keys.A:
				velocity_x = -10;
			break;
			case Keys.D:
				velocity_x = 10;
			break;
			case Keys.W:
				velocity_y = 10;
			break;
			case Keys.S:
				velocity_y = -10;
			break;
		}
		return false;
	}

	@Override
	protected boolean handleKeyUp(int keycode)
	{
		switch(keycode)
		{
			case Keys.A:
			case Keys.D:
				velocity_x = 0;
			break;
			case Keys.W:
			case Keys.S:
				velocity_y = 0;
			break;
		}
		return false;
	}
	
	@Override
	public void render(float delta)
	{
		super.render(delta);
		debugRenderer.render(world.getBox2DWorld(), camera.combined);
		
		batch.setProjectionMatrix(camera.combined.scl(1/Config.Box2D.PIXELS_PER_METER, 1/Config.Box2D.PIXELS_PER_METER, 1f));
		batch.begin();
		batch.setColor(Color.WHITE);
		player.getView().render(batch, delta);
		world.render(batch, delta);
		
		
		batch.setProjectionMatrix(hudCamera.combined);
		font.draw(batch, ""+player.getPosition().toString(), 10, 20);
		batch.end();
		
		update(delta);
	}

}
