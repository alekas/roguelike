package com.spadusa.roguelike.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.spadusa.roguelike.RoguelikeGame;
import com.spadusa.roguelike.view.tiles.Characters;

public class TitleScreen extends AbstractScreen
{
	private int currentOption = 0;
	private String[] menuItems = new String[] { "Play!", "Options", "Exit" };
	Characters chars = new Characters("font.png");

	public TitleScreen(RoguelikeGame game)
	{
		super(game);
	}

	@Override
	public void render(float delta)
	{
		super.render(delta);

		batch.begin();
		String title = "Roguelike";
		font.draw(batch, title, Gdx.graphics.getWidth() / 2 - font.getBounds(title).width / 2, Gdx.graphics.getHeight() / 2 + 100);
		drawMenu();
		for(int i=0; i<26; i++)
		{
			batch.setColor(Color.BLUE);
			batch.draw(this.chars.getBackgroundSprite(), 15+i*10, 15);
			batch.setColor(Color.CYAN);
			batch.draw(this.chars.getCharacterSprite((char)(i+65)), 15+i*10, 15);
			batch.setColor(Color.BLUE);
			batch.draw(this.chars.getBackgroundSprite(), 15+260+(i*10), 15);
			batch.setColor(Color.CYAN);
			batch.draw(this.chars.getCharacterSprite((char)(i+97)), 15+260+(i*10), 15);
		}
		for(int i=33; i<=64; i++)
		{
			//Gdx.app.log("W00t", ""+(char)i);
			batch.setColor(Color.BLUE);
			batch.draw(this.chars.getBackgroundSprite(), 15+(i-33)*10, 35);
			batch.setColor(Color.CYAN);
			batch.draw(this.chars.getCharacterSprite((char)(i)), 15+(i-33)*10, 35);
		}
		for(int i=123; i<=126; i++)
		{
			//Gdx.app.log("W00t", ""+(char)i);
			batch.setColor(Color.BLUE);
			batch.draw(this.chars.getBackgroundSprite(), 15+(i-123)*10, 55);
			batch.setColor(Color.CYAN);
			batch.draw(this.chars.getCharacterSprite((char)(i)), 15+(i-123)*10, 55);
		}
		batch.end();
	}

	private void drawMenu()
	{
		for (int i = 0; i < this.menuItems.length; i++)
		{
			String text = this.menuItems[i];
			float y = Gdx.graphics.getHeight() / 2 - 20 - i * (font.getBounds(text).height + 5);
			float x = Gdx.graphics.getWidth() - 200;
			if (currentOption == i)
			{
				font.setColor(Color.LIGHT_GRAY);
				font.draw(batch, ">", x - 15, y);
			}
			else
			{
				font.setColor(Color.WHITE);
			}
			font.draw(batch, text, x, y);
		}
	}

	@Override
	protected void update(float delta)
	{
	}

	@Override
	public void resize(int width, int height)
	{
	}

	@Override
	public void show()
	{
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}

	@Override
	public void dispose()
	{
	}

	@Override
	public boolean handleKeyDown(int keycode)
	{
		// TODO Auto-generated method stub
		if (keycode == Keys.DOWN)
		{
			TitleScreen.this.currentOption++;
		}
		else if (keycode == Keys.UP)
		{
			TitleScreen.this.currentOption--;
		}
		else if (keycode == Keys.ENTER)
		{
			switch (TitleScreen.this.currentOption)
			{
				case 0:
					TitleScreen.this.game.setScreen(TitleScreen.this.game.getGameScreen());
				break;
				case 1:
				// TODO
				break;
				case 2:
					TitleScreen.this.game.dispose();
					Gdx.app.exit();
				break;
			}
		}

		if (TitleScreen.this.currentOption < 0)
		{
			TitleScreen.this.currentOption = 0;
		}
		else if (TitleScreen.this.currentOption >= TitleScreen.this.menuItems.length)
		{
			TitleScreen.this.currentOption = TitleScreen.this.menuItems.length - 1;
		}
		return false;
	}
}
