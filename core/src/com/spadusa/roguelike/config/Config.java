package com.spadusa.roguelike.config;

public class Config
{
	public class Camera
	{
		public static final float WIDTH = 100;
		public static final float HEIGHT = 60;
	}
	public class World
	{
		public static final float WIDTH = 70;
		public static final float HEIGHT = 70;
	}
	
	public class Box2D
	{
		public static final float PIXELS_PER_METER = 10.0f;
		
	}
}
