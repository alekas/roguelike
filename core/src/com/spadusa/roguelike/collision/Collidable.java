package com.spadusa.roguelike.collision;

public interface Collidable
{
	public void collisionStart(Object other);
	public void collisionEnd(Object other);
}
