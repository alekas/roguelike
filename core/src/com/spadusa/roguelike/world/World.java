package com.spadusa.roguelike.world;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.spadusa.roguelike.model.Entity;
import com.spadusa.roguelike.view.Renderer;

public class World
{
	public ArrayList<Entity> walls = new ArrayList<Entity>();
	protected float width, height;
	protected com.badlogic.gdx.physics.box2d.World box2dWorld;
	
	public World(float width, float height)
	{
		this.width = width;
		this.height = height;
		
		this.box2dWorld = new com.badlogic.gdx.physics.box2d.World(new Vector2(0, 0), true);
		
		for(float i=0; i<width; i++)
		{
			Entity e = new Entity(this, '#');
			e.setColor(Color.RED);
			e.getController().initPhysics(i, 0, 1f, 1.7f, this, BodyType.StaticBody);
			walls.add(e);
			
			e = new Entity(this, '#');
			e.setColor(Color.PINK);
			e.getController().initPhysics(i, height*1.7f-1.7f, 1f, 1.7f, this, BodyType.StaticBody);
			walls.add(e);
		}
		for(float j=0; j<height; j++)
		{
			Entity e = new Entity(this, '#');
			e.setColor(Color.BLUE);
			e.getController().initPhysics(0, j*1.7f, 1f, 1.7f, this, BodyType.StaticBody);
			walls.add(e);
			
			e = new Entity(this, '#');
			e.setColor(Color.CYAN);
			e.getController().initPhysics(width-1, j*1.7f, 1f, 1.7f, this, BodyType.StaticBody);
			walls.add(e);
		}
		
	}
	
	public void render(SpriteBatch batch, float delta)
	{
		for(Entity e : walls)
		{
			e.getView().render(batch, delta);
		}
	}
	
	public void update(float delta)
	{
		box2dWorld.step(1/60f, 6, 2);
	}

	public float getWidth()
	{
		return this.width;
	}

	public float getHeight()
	{
		return this.height*1.7f;
	}

	public com.badlogic.gdx.physics.box2d.World getBox2DWorld()
	{
		return box2dWorld;
	}
}
