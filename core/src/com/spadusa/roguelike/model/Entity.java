package com.spadusa.roguelike.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.spadusa.roguelike.RoguelikeGame;
import com.spadusa.roguelike.config.Config;
import com.spadusa.roguelike.controller.EntityController;
import com.spadusa.roguelike.view.EntityRenderer;
import com.spadusa.roguelike.view.tiles.Tile;
import com.spadusa.roguelike.world.World;

public class Entity 
{
	protected Vector2 position = new Vector2();
	protected Vector2 velocity = new Vector2();
	protected EntityController controller;
	protected EntityRenderer view;
	protected Body body;
	
	public Entity(World world, char character)
	{
		controller = new EntityController(this);
		view = new EntityRenderer(this, 
				new Tile(RoguelikeGame.getCharacters().getCharacterSprite(character), 
				RoguelikeGame.getCharacters().getBackgroundSprite()));		
	}
	
	public Body getBody()
	{
		return body;
	}
	
	public void setBody(Body body)
	{
		this.body = body;
	}
	
	public EntityRenderer getView()
	{
		return view;
	}
	
	public EntityController getController()
	{
		return controller;
	}
	
	public Vector2 getPosition()
	{
		return this.body.getPosition();
	}

	public Vector2 getVelocity()
	{
		return this.velocity;
	}

	public void setVelocity(Vector2 velocity)
	{
		this.body.setLinearVelocity(velocity);
	}
	
	public void setVelocity(float x, float y)
	{
		this.body.setLinearVelocity(x, y);
	}

	public void setColor(Color color)
	{
		((EntityRenderer)this.getView()).getTile().setForegroundColor(color);
	}
}
