package com.spadusa.roguelike.view;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.spadusa.roguelike.model.Entity;
import com.spadusa.roguelike.view.tiles.Tile;

public class EntityRenderer extends Renderer<Entity>
{
	protected Tile tile;
	public EntityRenderer(Entity entity, Tile tile)
	{
		super(entity);
		this.tile = tile;
	}
	
	public Tile getTile()
	{
		return this.tile;
	}
	
	@Override
	public void render(SpriteBatch batch, float delta)
	{
		tile.render(batch, delta, model.getPosition().x, model.getPosition().y);
	}
}
