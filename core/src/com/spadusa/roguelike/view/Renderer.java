package com.spadusa.roguelike.view;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Renderer<T>
{
	protected T model;
	public Renderer(T model)
	{
		this.model = model;
	}
	
	public abstract void render(SpriteBatch batch, float delta);
}
