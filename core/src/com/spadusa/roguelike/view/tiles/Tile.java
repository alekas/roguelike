package com.spadusa.roguelike.view.tiles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.spadusa.roguelike.config.Config;

public class Tile
{
	protected TextureRegion sprite;
	protected TextureRegion background;
	protected Color foregroundColor = Color.WHITE;
	protected Color backgroundColor = Color.CLEAR;
	
	public Tile(TextureRegion sprite, TextureRegion background)
	{
		this.sprite = sprite;
		this.background = background;
	}
	
	public void render(SpriteBatch batch, float delta, float x, float y)
	{
		batch.setColor(this.backgroundColor);
		if(backgroundColor != Color.CLEAR)
		{
			batch.draw(background, x*Config.Box2D.PIXELS_PER_METER, y*Config.Box2D.PIXELS_PER_METER);
		}
		batch.setColor(this.foregroundColor);
		batch.draw(sprite, x*10-5, y*10-9f);
	}

	public void setForegroundColor(Color color)
	{
		this.foregroundColor = color;
	}

	public void setBackgroundColor(Color color)
	{
		this.backgroundColor = color;
	}
}
