package com.spadusa.roguelike.view.tiles;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Characters
{
	protected HashMap<String, TextureRegion> characters = new HashMap<String, TextureRegion>();
	protected Texture fontsheet;
	protected final int CHARACTER_WIDTH=10, CHARACTER_HEIGHT=18;
	public Characters(String fontsheet_filename)
	{
		this.fontsheet = new Texture(Gdx.files.internal(fontsheet_filename));
	}
	
	public TextureRegion getCharacterSprite(char character)
	{
		String key = String.valueOf(character);
		if(characters.get(key)==null)
		{
			int x=0, y=0, char_code = (int)character;
			if((char_code >= 65 && char_code <=90) || (char_code>=97 && char_code<=122))
			{
				if(char_code < 97) //Upper case
				{
					x = (char_code-65)%24;
					y = (int) Math.floor((char_code-65)/24);
				}
				else
				{
					x = (char_code-97+2)%24;
					y = (int)Math.floor((char_code-97+2)/24)+1;
				}
			}
			else if(char_code >= 33 && char_code <= 64)
			{
				x = (char_code-33+4)%24;
				y = (int)Math.floor((char_code-33+4)/24)+2;
			}
			else if(char_code >=123)
			{
				x = (char_code-123+18);
				y = 3;
			}
			
			x *= this.CHARACTER_WIDTH;
			y *= this.CHARACTER_HEIGHT;
			TextureRegion region = new TextureRegion(this.fontsheet, x, y, this.CHARACTER_WIDTH, this.CHARACTER_HEIGHT);
			characters.put(key, region);
		}
		
		return characters.get(key);
	}
	
	public TextureRegion getBackgroundSprite()
	{
		String key = String.valueOf((char)0);
		if(characters.get(key)==null)
		{
			characters.put(key, new TextureRegion(this.fontsheet, 240, 108, this.CHARACTER_WIDTH, this.CHARACTER_HEIGHT));
		}
		
		return characters.get(key);
	}
}
