package com.spadusa.roguelike;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.spadusa.roguelike.screen.GameScreen;
import com.spadusa.roguelike.screen.TitleScreen;
import com.spadusa.roguelike.view.tiles.Characters;

public class RoguelikeGame extends Game
{
	SpriteBatch batch;
	BitmapFont font;
	protected static Characters chars;

	@Override
	public void create()
	{
		batch = new SpriteBatch();
		font = new BitmapFont();
		
		RoguelikeGame.chars = new Characters("font.png");
		
		setScreen(new TitleScreen(this));
	}
	
	public static Characters getCharacters()
	{
		return chars;
	}

	@Override
	public void render()
	{
		super.render();
	}

	public Screen getGameScreen()
	{
		return new GameScreen(this);
	}

	public Screen getTitleScreen()
	{
		return new TitleScreen(this);
	}
}
