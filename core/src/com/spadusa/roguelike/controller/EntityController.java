package com.spadusa.roguelike.controller;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.spadusa.roguelike.config.Config;
import com.spadusa.roguelike.model.Entity;
import com.spadusa.roguelike.util.Util;
import com.spadusa.roguelike.world.World;

public class EntityController extends Controller
{
	protected Entity entity;
	
	public EntityController(Entity entity)
	{
		this.entity = entity;
	}
	
	public void initPhysics(float x, float y, float width, float height, World world, BodyType bodyType)
	{
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = bodyType;
		bodyDef.position.set(x+.5f, y+.5f);
		
		Body body;
		body = world.getBox2DWorld().createBody(bodyDef);
		body.setLinearDamping(3);
		body.setUserData(entity);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width/2f, height/2f);
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 32.5f;
		fixtureDef.friction = .3f;
		fixtureDef.restitution = 0.1f;
		
		body.createFixture(fixtureDef);
		body.setFixedRotation(true);
		
		entity.setBody(body);
		shape.dispose();
	}

	@Override
	public void update(float delta)
	{
	}
}
