package com.spadusa.roguelike.controller;

public abstract class Controller
{
	public abstract void update(float delta);
}
